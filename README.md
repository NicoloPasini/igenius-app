# iGenius Application

This iOS Application display the list of the pictures uploaded by the user on his Instagram Account. 

## Getting Started

To get the most updated version of the code, download the last commit in the master branch.

### Prerequisites

This project has been created using XCode 11.0.

## Organization of the Project

iGenius is the main target, which is structured in the following way:

* Gallery Page: this folder contain all the files related to the page which displays the list of pictures. Each file is added to a subfolder organized by the file types (e.g. Views, ViewControllers, Xibs, ...);
* Dependency Injection: is the folder containing the definitions of the services that will be injected, their registration and mock for UI tests;
* Login: contains the implementation of the LoginService
* Network: this folder contains the implementation of the Network Connection Service which monitors the availability of the network connection;
* Utilities: in this folder you can find some Extensions and the definition of the Errors used.

Inside the main target, two Test targets are included: iGeniusTest which contains the implementation of the Unit Tests and iGeniusUITest which contains the implementation of UI Tests.

## Dependencies ##
The dependencies are managed using both CocoaPods and Swift Package Manager. The two Swift Packages included are the following packages:

* [OSLogger](https://github.com/NPasini/OSLogger): which contains my custom implementation of a logger based on os.log;
* [Network Manager](https://github.com/NPasini/NetworkManager): which is my custom implementation of the Network Manager based on URLSession.

## Author ##

**Nicolò Pasini**



