//
//  NetworkNotAvailableTest.swift
//  iGeniusUITests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import XCTest

class NetworkNotAvailableTest: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launchArguments.append("--connectionNotAvailable-uitesting")
        
        app.launch()
    }
    
    func testLoggedOutLaunch() {
        let startVC = app.otherElements["GalleryViewController"]
        XCTAssertTrue(startVC.exists)
        
        let messageView = startVC.otherElements["messageView"]
        XCTAssertTrue(messageView.exists)
        let label = messageView.staticTexts.element(matching:.any, identifier: "messageLabel")
        XCTAssertTrue(label.exists)
        XCTAssertEqual(label.label, "Network connection not available")
        
        let collectionView = startVC.collectionViews["galleryCollectionView"]
        XCTAssertTrue(collectionView.exists)
        
        let segmentedControl = startVC.segmentedControls.matching(identifier: "segmentedControl").element
        XCTAssertTrue(segmentedControl.exists)
    }
}
