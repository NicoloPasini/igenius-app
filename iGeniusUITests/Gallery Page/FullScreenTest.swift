//
//  FullScreenTest.swift
//  iGeniusUITests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import XCTest

class FullScreenTest: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launchArguments.append("--uitesting")
        
        app.launch()
    }
    
    func testTapOnCell() {
        let startVC = app.otherElements["GalleryViewController"]
        let collectionView = startVC.collectionViews["galleryCollectionView"]
        
        let firstCell = collectionView.cells.firstMatch
        firstCell.tap()
        
        let fullScreenCollectionView = startVC.collectionViews["fullScreenCollectionView"]
        XCTAssertTrue(fullScreenCollectionView.exists)
        
        let cell = fullScreenCollectionView.cells.firstMatch
        XCTAssertTrue(cell.exists)
    }
    
    func closeFullScreenSwipingDown() {
        let startVC = app.otherElements["GalleryViewController"]
        let collectionView = startVC.collectionViews["galleryCollectionView"]
        
        let firstCell = collectionView.cells.firstMatch
        firstCell.tap()
        
        startVC.swipeDown()
        
        let fullScreenCollectionView = startVC.collectionViews["fullScreenCollectionView"]
        XCTAssertFalse(fullScreenCollectionView.exists)
        XCTAssertTrue(collectionView.exists)
    }
    
    func closeFullScreenSwipingUp() {
        let startVC = app.otherElements["GalleryViewController"]
        let collectionView = startVC.collectionViews["galleryCollectionView"]
        
        let firstCell = collectionView.cells.firstMatch
        firstCell.tap()
        
        startVC.swipeUp()
        
        let fullScreenCollectionView = startVC.collectionViews["fullScreenCollectionView"]
        XCTAssertFalse(fullScreenCollectionView.exists)
        XCTAssertTrue(collectionView.exists)
    }
}

