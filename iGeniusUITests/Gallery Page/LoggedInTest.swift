//
//  LoggedInTest.swift
//  iGeniusUITests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import XCTest

class LoggedInTest: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launchArguments.append("--uitesting")
        
        app.launch()
    }
    
    func testLoggedInLaunch() {
        let startVC = app.otherElements["GalleryViewController"]
        XCTAssertTrue(startVC.exists)
        
        let collectionView = startVC.collectionViews["galleryCollectionView"]
        XCTAssertTrue(collectionView.exists)
        XCTAssertTrue(collectionView.cells.count == 3)
        
        let segmentedControl = startVC.segmentedControls.matching(identifier: "segmentedControl").element
        XCTAssertTrue(segmentedControl.exists)
        
        let firstCell = collectionView.cells.firstMatch
        let likes = firstCell.staticTexts.element(matching:.any, identifier: "likesLabel")
        XCTAssertEqual(likes.label, "10")
        let comments = firstCell.staticTexts.element(matching:.any, identifier: "commentsLabel")
        XCTAssertEqual(comments.label, "25")
    }
}
