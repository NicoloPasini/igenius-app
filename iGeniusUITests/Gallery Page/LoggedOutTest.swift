//
//  LoggedOutTest.swift
//  iGeniusUITests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import XCTest

class LoggedOutTest: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        app = XCUIApplication()
        app.launchArguments.append("--logout-uitesting")
        
        app.launch()
    }
    
    func testLoggedOutLaunch() {
        let startVC = app.otherElements["GalleryViewController"]
        XCTAssertTrue(startVC.exists)
        
        let webView = startVC.webViews.matching(identifier: "loginWebView").element
        XCTAssertTrue(webView.exists)
    }
}
