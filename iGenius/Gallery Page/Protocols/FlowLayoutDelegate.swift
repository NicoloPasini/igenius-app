//
//  FlowLayoutDelegate.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 11/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit

protocol FlowLayoutDelegate: class {
    func collectionView(_ collectionView: UICollectionView, ratioForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat
}
