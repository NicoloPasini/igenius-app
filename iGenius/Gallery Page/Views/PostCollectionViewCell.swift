//
//  PostCollectionViewCell.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 07/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import SDWebImage

class PostCollectionViewCell: UICollectionViewCell, Identifiable {
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var comments: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var heartImageView: UIImageView!
    @IBOutlet weak var messageImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        likes.text = ""
        comments.text = ""
        imageView.image = nil
    }
    
    //MARK: Public Functions
    func configure(with post: Post, isStaggered: Bool) {
        likes.text = String(post.likes)
        comments.text = String(post.comments)
        
        let urlString = isStaggered ? post.lowResImage.url : post.thumbImage.url
        
        if let imageURL = URL(string: urlString) {
            imageView.sd_setImage(with: imageURL) { [weak self] (image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
                guard error == nil else { return }
                
                self?.imageView.image = image
            }
        } else {
            imageView.image = UIImage()
        }
    }
}

extension PostCollectionViewCell: SizeableCollectionCell {
    static var estimatedSize: CGSize {
        let dimension = UIScreen.main.bounds.width/3
        return CGSize(width: dimension, height: dimension)
    }
}
