//
//  ImageCollectionViewCell.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 08/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCollectionViewCell: UICollectionViewCell, Identifiable {
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var temporaryImageView: UIImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        caption.text = ""
        imageView.image = nil
        temporaryImageView.isHidden = true
    }
    
    //MARK: Public Functions
    func configure(with post: Post, ratio: CGFloat, selectedSegment: Int) {
        caption.text = post.caption ?? ""
        imageViewHeight.constant = UIScreen.main.bounds.width * ratio
        
        let temporaryImage = selectedSegment == 0 ? post.lowResImage.url : post.thumbImage.url
        setTemporaryImage(urlString: temporaryImage)
        
        if let imageURL = URL(string: post.standardResImage.url) {
            imageView.sd_setImage(with: imageURL) { [weak self] (image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
                guard error == nil else { return }
                
                self?.imageView.image = image
                self?.temporaryImageView.isHidden = true
            }
        } else {
            imageView.image = UIImage()
        }
    }

    //MARK: Private Functions
    private func setTemporaryImage(urlString: String) {
        if let imageURL = URL(string: urlString) {
            temporaryImageView.isHidden = false
            temporaryImageView.sd_setImage(with: imageURL, completed: nil)
        } else {
            temporaryImageView.isHidden = true
        }
    }
}

extension ImageCollectionViewCell: SizeableCollectionCell {
    static var estimatedSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 500)
    }
}

