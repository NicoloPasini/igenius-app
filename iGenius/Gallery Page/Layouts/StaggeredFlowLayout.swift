//
//  StaggeredFlowLayout.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 08/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import Foundation

class StaggeredFlowLayout: UICollectionViewLayout {
    weak var delegate: FlowLayoutDelegate?
    
    private let numberOfColumns = 2
    private var contentWidth: CGFloat = 0
    private var contentHeight: CGFloat = 0
    private var cache: [UICollectionViewLayoutAttributes] = []
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        guard cache.isEmpty, let collectionView = collectionView else { return }
        
        contentWidth = collectionView.bounds.width
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        
        var xOffset: [CGFloat] = []
        var yOffset: [CGFloat] = .init(repeating: 0, count: numberOfColumns)
        
        for column in 0..<numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        
        var column = 0
        
        if collectionView.numberOfSections > 0 && collectionView.numberOfItems(inSection: 0) > 0 {
            for item in 0..<collectionView.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(item: item, section: 0)
                
                let ratio = delegate?.collectionView(collectionView, ratioForPhotoAtIndexPath: indexPath) ?? 1
                let height = columnWidth * ratio
                let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
                
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame                
                cache.append(attributes)
                
                contentHeight = max(contentHeight, frame.maxY)
                yOffset[column] = yOffset[column] + height
                
                column = column < (numberOfColumns - 1) ? (column + 1) : 0
            }
        } else {
            cache = []
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
    //MARK: Public Functions
    func invalidate() {
        cache = []
        invalidateLayout()
    }
}
