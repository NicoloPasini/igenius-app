//
//  GridFlowLayout.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 08/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit

class GridFlowLayout: UICollectionViewFlowLayout {
    
    override func prepare() {
        super.prepare()

        guard let _ = collectionView else { return }
        
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0
        
        self.itemSize = PostCollectionViewCell.estimatedSize
    }
}
