//
//  FullScreenFlowLayout.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 09/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit

class FullScreenFlowLayout: UICollectionViewFlowLayout {
    private var contentWidth: CGFloat = 0
    private var contentHeight: CGFloat = 0
    private var cache: [UICollectionViewLayoutAttributes] = []
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        super.prepare()

        guard cache.isEmpty, let collectionView = collectionView else { return }
        
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0
        
        contentWidth = 0
        contentHeight = collectionView.bounds.height
        
        if collectionView.numberOfSections > 0 && collectionView.numberOfItems(inSection: 0) > 0 {
            for item in 0..<collectionView.numberOfItems(inSection: 0) {
                let indexPath = IndexPath(item: item, section: 0)
                
                contentWidth += UIScreen.main.bounds.width
        
                let width = UIScreen.main.bounds.width
                let height = UIScreen.main.bounds.height
                let yOffset = (contentHeight - height)/2
                let xOffset = UIScreen.main.bounds.width * CGFloat(item)
                let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
                
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
            }
        } else {
            cache = []
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes: [UICollectionViewLayoutAttributes] = []
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
}
