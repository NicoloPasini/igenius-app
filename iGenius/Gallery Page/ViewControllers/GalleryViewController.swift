//
//  GalleryViewController.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 30/09/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import WebKit
import Rswift
import RxSwift
import OSLogger
import ReactiveSwift
import RxDataSources

class GalleryViewController: UIViewController {
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var fullScreenCollectionView: UICollectionView!
    @IBOutlet weak var messageBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageViewHeightConstraint: NSLayoutConstraint!
    
    private var webView: WKWebView!
    private var isStaggered: Bool = true
    private var viewModel: GalleryViewModel?
    private var selectedIndexPath: IndexPath?
    private var lowResImagesRatio: [CGFloat] = []
    private var selectedCell: UICollectionViewCell?
    private var standardResImagesRatio: [CGFloat] = []
    
    //Reactive properties
    private let disposeBag = DisposeBag()
    private var compositeDisposable = ReactiveSwift.CompositeDisposable()
    
    //CollectionView Layouts
    private let gridLayout = GridFlowLayout()
    private let staggeredLayout = StaggeredFlowLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = GalleryViewModel()
        bindWithViewModel()
        
        setTitle()
        configureWebView()
        configureMessageView()
        configureCollectionView()
        configureSegmentedControl()
        configureFullScreenCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        OSLogger.uiLog(message: "Showing Gallery ViewController")
    }
    
    deinit {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
    
    //MARK: Actions
    @IBAction func swipeGestureRecognizerHandler(_ sender: UISwipeGestureRecognizer) {
        collapseCollectionView()
    }
    
    //MARK: Private Functions
    private func setTitle() {
        let fullTitle = NSMutableAttributedString(attributedString: NSAttributedString(string: "Insta", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25, weight: .light), NSAttributedString.Key.foregroundColor: UIColor.white]))
        fullTitle.append(NSAttributedString(string: " Gallery", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor.white]))
        
        let titleLabel = UILabel()
        titleLabel.attributedText = fullTitle
        self.navigationItem.titleView = titleLabel
    }
    
    private func configureWebView() {
        webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        webView.accessibilityIdentifier = "loginWebView"
        
        webView.navigationDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            webView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func configureMessageView() {
        messageView.layer.cornerRadius = 10
        messageBottomConstraint.constant = 0
        messageViewHeightConstraint.constant = 0
        messageLabel.text = R.string.localized.networkMessage()
    }
    
    private func configureCollectionView() {
        collectionView.delegate = self
        collectionView.indicatorStyle = .white
        collectionView.register(viewType: PostCollectionViewCell.self)
        
        staggeredLayout.delegate = self
        
        let dataSource = RxCollectionViewSectionedReloadDataSource<PostSection>(configureCell: { [weak self] (dataSource, collectionView, indexPath, postItem) -> UICollectionViewCell in
            let section = dataSource.sectionModels[indexPath.section]
            let post = section.items[indexPath.row]
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PostCollectionViewCell.identifier, for: indexPath) as? PostCollectionViewCell {
                cell.configure(with: post, isStaggered: self?.isStaggered ?? false)
                return cell
            } else {
                return UICollectionViewCell()
            }
        })
        
        viewModel?.dataSource.bind(to: collectionView.rx.items(dataSource: dataSource)).disposed(by: disposeBag)
    }
    
    private func configureFullScreenCollectionView() {
        fullScreenCollectionView.isPagingEnabled = true
        fullScreenCollectionView.register(viewType: ImageCollectionViewCell.self)
        fullScreenCollectionView.setCollectionViewLayout(FullScreenFlowLayout(), animated: false)
        
        let fullScreenDataSource = RxCollectionViewSectionedReloadDataSource<PostSection>(configureCell: { [weak self] (dataSource, collectionView, indexPath, postItem) -> UICollectionViewCell in
            let section = dataSource.sectionModels[indexPath.section]
            let post = section.items[indexPath.row]
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as? ImageCollectionViewCell {
                cell.configure(with: post, ratio: self?.standardResImagesRatio[indexPath.row] ?? 1, selectedSegment: self?.segmentedControl.selectedSegmentIndex ?? 0)
                return cell
            } else {
                return UICollectionViewCell()
            }
        })
        
        viewModel?.dataSource.bind(to: fullScreenCollectionView.rx.items(dataSource: fullScreenDataSource)).disposed(by: disposeBag)
    }
    
    private func configureSegmentedControl() {
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .regular), NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
        segmentedControl.rx.selectedSegmentIndex.subscribe(onNext: { [weak self] (index: Int) in
            OSLogger.uiLog(message: "Switched to index \(index) in segmented control")
            
            self?.isStaggered = index == 0 ? true : false
            if let layout = index == 0 ? self?.staggeredLayout : self?.gridLayout {
                self?.collectionView.setCollectionViewLayout(layout, animated: true)
            }
        }).disposed(by: disposeBag)
    }
    
    private func bindWithViewModel() {
        let vmObservationQueueScheduler = QueueScheduler(qos: .userInteractive, name: "VMObservationQueue")
        if let vm = viewModel {
            compositeDisposable += vm.status.producer.observe(on: vmObservationQueueScheduler).startWithValues({ [weak self] (status: GalleryStatus) in
                OSLogger.dataFlowLog(message: "Actual status: \(String(describing: status))")
                switch status {
                case .initial, .fetchingServerData:
                    self?.hideWebView()
                    self?.hideMessageView()
                case .userNotLogged:
                    self?.hideMessageView()
                    self?.loadLogin()
                case .fetchingDbData, .networkNotAvailable:
                    self?.hideWebView()
                    self?.showMessageView()
                }
            })
            
            vm.dataSource.observeOn(ConcurrentDispatchQueueScheduler(qos: .userInteractive)).subscribe(onNext: { [weak self] (postsSection: [PostSection]) in
                self?.lowResImagesRatio = postsSection[0].items.map({ (post: Post) -> CGFloat in
                    return CGFloat(post.lowResImage.height)/CGFloat(post.lowResImage.width)
                })
                self?.standardResImagesRatio = postsSection[0].items.map({ (post: Post) -> CGFloat in
                    return CGFloat(post.standardResImage.height)/CGFloat(post.standardResImage.width)
                })
                self?.staggeredLayout.invalidate()
                OSLogger.uiLog(message: "Invalidating Staggered Layout after new data has been retrieved")
                }, onError: { [weak self] _ in
                    self?.lowResImagesRatio = []
                    self?.standardResImagesRatio = []
            }).disposed(by: disposeBag)
        }
    }
    
    private func hideWebView() {
        DispatchQueue.main.async {
            OSLogger.uiLog(message: "Hiding webview")
            self.webView.isHidden = true
        }
    }
    
    private func loadLogin() {
        DispatchQueue.main.async {
            self.webView.isHidden = false
            if let url = self.viewModel?.authUrl {
                OSLogger.uiLog(message: "Starting authentication flow")
                self.webView.load(URLRequest(url: url))
            }
        }
    }
    
    private func showMessageView() {
        OSLogger.uiLog(message: "Showing message")
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3, animations: {
                self.messageBottomConstraint.constant = 5
                self.messageViewHeightConstraint.constant = 45
                self.view.layoutIfNeeded()
            })
        }
    }
    
    private func hideMessageView() {
        OSLogger.uiLog(message: "Hiding message")
        if messageViewHeightConstraint.constant != 0 {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
                    self.messageBottomConstraint.constant = 0
                    self.messageViewHeightConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    private func expandCollectionView() {
        OSLogger.uiLog(message: "Showing fullscreen CollectionView")
        
        if let cell = selectedCell {
            let xCenter = cell.frame.minX + cell.frame.width/2
            let yCenter = cell.frame.minY + cell.frame.height/2 - collectionView.contentOffset.y
            
            let xTransaltion = view.center.x - xCenter
            let yTransaltion = fullScreenCollectionView.bounds.height/2 - yCenter - collectionView.frame.minY

            let translation = cell.transform.translatedBy(x: xTransaltion, y: yTransaltion)

            var ratio: CGFloat = 1
            if let selectedIndex = selectedIndexPath?.row {
                ratio = standardResImagesRatio[selectedIndex]
            }
            
            let scaleXFactor = UIScreen.main.bounds.width/cell.frame.width
            let scaleYFactor = (UIScreen.main.bounds.width * ratio)/cell.frame.height
            let scaledTransformation = translation.scaledBy(x: scaleXFactor, y: scaleYFactor)
            
            self.fullScreenCollectionView.alpha = 0
            self.fullScreenCollectionView.isHidden = false
            self.fullScreenCollectionView.layer.zPosition = 2
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                cell.transform = scaledTransformation
                
                if let postCell = cell as? PostCollectionViewCell {
                    postCell.likes.alpha = 0
                    postCell.comments.alpha = 0
                    postCell.heartImageView.alpha = 0
                    postCell.messageImageView.alpha = 0
                }
                
                self.fullScreenCollectionView.alpha = 1
            }) 
        }
    }
    
    private func collapseCollectionView() {
        OSLogger.uiLog(message: "Collapsing fullscreen CollectionView")
        
        if let cell = selectedCell {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                cell.transform = .identity
                self.fullScreenCollectionView.isHidden = true
                self.fullScreenCollectionView.alpha = 0
                
                if let postCell = cell as? PostCollectionViewCell {
                    postCell.likes.alpha = 1
                    postCell.comments.alpha = 1
                    postCell.heartImageView.alpha = 1
                    postCell.messageImageView.alpha = 1
                }
            }) { (completed: Bool) in
                if completed {
                    self.selectedCell?.layer.zPosition = 0
                    self.selectedCell = nil
                    self.selectedIndexPath = nil
                    
                    self.fullScreenCollectionView.layer.zPosition = 0
                }
            }
        }
    }
}

//MARK: UICollectionViewDelegate
extension GalleryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        OSLogger.uiLog(message: "Tapped on cell \(indexPath.row)")
        
        selectedIndexPath = indexPath
        selectedCell = collectionView.cellForItem(at: indexPath)
        
        fullScreenCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)

        //Setting the zPosition of the selected cell in order to have it move on top of the other cells
        selectedCell?.layer.zPosition = 1
        
        expandCollectionView()
    }
}

//MARK: UICollectionViewDelegate
extension GalleryViewController: FlowLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, ratioForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        if lowResImagesRatio.count != 0, lowResImagesRatio.count >= indexPath.row {
            return lowResImagesRatio[indexPath.row]
        } else {
            return 1
        }
    }
}

//MARK: WKNavigationDelegate
extension GalleryViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        
        if let urlString = navigationAction.request.url?.absoluteString, let authorizedURL = viewModel?.authorizedUrl, let tokenRegex = viewModel?.tokenRegex, urlString.contains(authorizedURL) {
            if let range = urlString.range(of: tokenRegex, options: .regularExpression) {
                let components = urlString[range].components(separatedBy: "=")
                if components.count == 2 {
                    decisionHandler(.cancel)
                    webView.stopLoading()
                    viewModel?.saveUserToken(token: components[1])
                    OSLogger.uiLog(message: "Completed authentication flow")
                    return
                }
            }
        }
        
        decisionHandler(.allow)
    }
}
