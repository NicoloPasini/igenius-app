//
//  WebPostsRepository.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 06/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation
import ReactiveSwift
import NetworkManager

class WebPostsRepository: WebPostsRepositoryService {
    //MARK: Public Functions
    public func getPostsFromServer() -> SignalProducer<[Post], NSError> {
        let request = RecentMediaRequest()
        return observableForRecentMedia(request)
    }
    
    //MARK: Private Functions
    private func observableForRecentMedia(_ request: RecentMediaRequest) -> SignalProducer<[Post], NSError> {
        return SignalProducer { (observer, lifetime) in
            let subscription = APIPerformer.shared.performApi(request, QoS: .userInitiated, completionQueue: .global(qos: .userInitiated)) { (result: Result<RecentMediaResponse, NSError>) in
                
                switch result {
                case .success(let response):
                    observer.send(value: response.getPosts())
                    observer.sendCompleted()
                case .failure(let error):
                    observer.send(error: error)
                }
            }
            
            lifetime.observeEnded {
                subscription.dispose()
            }
        }
    }
}
