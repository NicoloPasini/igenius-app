//
//  PostsRepository.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import RxSwift
import OSLogger
import Foundation
import ReactiveSwift

class PostsRepository: PostsRepositoryService {
    let dataSource: Observable<[Post]>
    weak var expirationDelegate: ExpirationDelegate?
    
    private let disposeBag = DisposeBag()
    private var actualDbDatasource: [Post] = []
    private let searialDisposable = ReactiveSwift.SerialDisposable(nil)
    private let subscriptionScheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
    private let databaseQueueScheduler = QueueScheduler(qos: .userInitiated, name: "PostServerQueue")
    
    //DI Properties
    private let dbComponent: DatabaseService?
    private let webRepository: WebPostsRepositoryService?
    
    init() {
        dbComponent = AssemblerWrapper.shared.resolve(DatabaseService.self)
        webRepository = AssemblerWrapper.shared.resolve(WebPostsRepositoryService.self)
        
        if let dbManager = dbComponent {
            dataSource = dbManager.getPosts()
        } else {
            OSLogger.dependencyInjectionLog(message: "Unable to resolve DatabaseService dependency")
            dataSource = Observable.just([])
        }
        
        dataSource.subscribeOn(subscriptionScheduler).subscribe(onNext: { [weak self] (posts: [Post]) in
            self?.actualDbDatasource = posts
            }, onError: { [weak self] _ in
                self?.actualDbDatasource = []
        }).disposed(by: disposeBag)
    }
    
    deinit {
        if !searialDisposable.isDisposed {
            searialDisposable.dispose()
        }
    }
    
    //MARK: Public Functions
    public func getPostsFromWebService() {
        if let webRepository = self.webRepository {
            searialDisposable.inner = webRepository.getPostsFromServer().observe(on: databaseQueueScheduler).on(failed: { [weak self] (error: NSError) in
                OSLogger.errorLog(message: "Failed to get Posts from backend, error: \(error.description)")
                if let self = self, let e = error as? AppError, e.code == AppErrorType.tokenInvalid.rawValue {
                    self.expirationDelegate?.expireToken()
                }
            }, value: { [weak self] (posts: [Post]) in
                OSLogger.dataFlowLog(message: "Received \(posts.count) Posts from backend")
                if let self = self {
                    self.savePosts(posts)
                }
            }).start()
        } else {
            OSLogger.dependencyInjectionLog(message: "Unable to resolve WebPostsRepository dependency")
        }
    }
    
    //Private Functions
    private func diffDataSource(dbDataSource: [Post], serverDataSource: [Post]) -> [Post] {
        return serverDataSource.filter { (post: Post) -> Bool in
            !dbDataSource.contains { (p: Post) -> Bool in
                p == post
            }
        }
    }
    
    private func savePosts(_ posts: [Post]) {
        if let dbManager = self.dbComponent {
            let postToPersist = self.diffDataSource(dbDataSource: self.actualDbDatasource, serverDataSource: posts)
            if postToPersist.count > 0 {
                OSLogger.databaseLog(message: "Saving \(postToPersist.count) Posts on database")
                do {
                    try dbManager.save(posts: postToPersist).subscribeOn(subscriptionScheduler).subscribe().disposed(by: self.disposeBag)
                    OSLogger.databaseLog(message: "Saved Posts on database")
                } catch {
                    OSLogger.errorLog(message: "Failed to save Posts on database, error: \(error)")
                }
            }
        } else {
            OSLogger.dependencyInjectionLog(message: "Unable to resolve DatabaseService dependency")
        }
    }
}
