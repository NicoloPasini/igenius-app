//
//  PostsSection.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 07/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation
import RxDataSources

struct PostSection {
    var items: [Item]
}

extension PostSection: SectionModelType {
    typealias Item = Post
    
    init(original: PostSection, items: [Item]) {
        self = original
        self.items = items
    }
}
