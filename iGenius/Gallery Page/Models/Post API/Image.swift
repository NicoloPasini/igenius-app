//
//  Image.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 01/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import Foundation

struct Image {
    let width: Int
    let height: Int
    let url: String
    
    init(width: Int, height: Int, url: String) {
        self.url = url
        self.width = width
        self.height = height
    }
}

extension Image: Decodable {
    enum CodingKeys: CodingKey {
        case id
        case url
        case width
        case height
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        url = try container.decode(String.self, forKey: .url)
        width = try container.decode(Int.self, forKey: .width)
        height = try container.decode(Int.self, forKey: .height)
    }
}
