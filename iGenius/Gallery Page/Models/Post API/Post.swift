//
//  Post.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 01/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import Foundation

struct Post {
    let likes: Int
    let type: String
    let comments: Int
    let postId: String
    let caption: String?
    let thumbImage: Image
    let lowResImage: Image
    let standardResImage: Image
    
    init(likes: Int,
         type: String,
         comments: Int,
         postId: String,
         caption: String?,
         thumbImage: Image,
         lowResImage: Image,
         standardResImage: Image) {
        
        self.type = type
        self.likes = likes
        self.postId = postId
        self.caption = caption
        self.comments = comments
        self.thumbImage = thumbImage
        self.lowResImage = lowResImage
        self.standardResImage = standardResImage
    }
}

extension Post: Decodable {
    enum CodingKeys: CodingKey {
        case id
        case type
        case likes
        case images
        case caption
        case comments
    }
    
    enum CountCodingKeys: CodingKey {
        case count
    }
    
    enum CaptionCodingKeys: CodingKey {
        case text
    }
    
    enum ImagesCodingKeys: CodingKey {
        case thumbnail
        case low_resolution
        case standard_resolution
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        postId = try container.decode(String.self, forKey: .id)
        type = try container.decode(String.self, forKey: .type)
        
        //Caption
        let captionContainer = try? container.nestedContainer(keyedBy: CaptionCodingKeys.self, forKey: .caption)
        if let captionNestedContainer = captionContainer {
            caption = try captionNestedContainer.decodeIfPresent(String.self, forKey: .text)
        } else {
            caption = nil
        }
        
        //Likes
        var countContainer = try container.nestedContainer(keyedBy: CountCodingKeys.self, forKey: .likes)
        likes = try countContainer.decode(Int.self, forKey: .count)
        
        //Comments
        countContainer = try container.nestedContainer(keyedBy: CountCodingKeys.self, forKey: .comments)
        comments = try countContainer.decode(Int.self, forKey: .count)
        
        //Images
        let imagesContainer = try container.nestedContainer(keyedBy: ImagesCodingKeys.self, forKey: .images)
        thumbImage = try imagesContainer.decode(Image.self, forKey: .thumbnail)
        lowResImage = try imagesContainer.decode(Image.self, forKey: .low_resolution)
        standardResImage = try imagesContainer.decode(Image.self, forKey: .standard_resolution)
    }
}

extension Post: Equatable {
    static func ==(lhs: Post, rhs: Post) -> Bool {
        return lhs.postId == rhs.postId
    }
}
