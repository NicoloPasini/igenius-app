//
//  GalleryStatus.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 07/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation

enum GalleryStatus {
    case initial
    case userNotLogged
    case fetchingDbData
    case fetchingServerData
    case networkNotAvailable
    
    var description: String {
        switch self {
        case .initial: return "Initial"
        case .userNotLogged: return "User not logged"
        case .fetchingDbData: return "Fetching data stored in DB"
        case .fetchingServerData: return "Fetching data from backend"
        case .networkNotAvailable: return "Network connection not available"
        }
    }
}
