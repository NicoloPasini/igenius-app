//
//  GalleryViewModel.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 30/09/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import RxSwift
import OSLogger
import Foundation
import RxDataSources
import ReactiveSwift

typealias BoolSignal = Signal<Bool, Never>

class GalleryViewModel {
    private let disposeBag = DisposeBag()
    private var disposable: ReactiveSwift.Disposable?
    private(set) var dataSource: Observable<[PostSection]>
    private(set) var status: MutableProperty<GalleryStatus>
    private let loggedUserPipe: (output: BoolSignal, input: BoolSignal.Observer)
    private let subscriptionScheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
    
    //DI properties
    private let loginManager: LoginService?
    private let databaseManager: DatabaseService?
    private let postsRepository: PostsRepositoryService?
    
    var authUrl: URL? {
        get {
            return loginManager?.authenticationURL
        }
    }
    
    var tokenRegex: String {
        get {
            return loginManager?.tokenRegex ?? ""
        }
    }
    
    var authorizedUrl: String {
        get {
            return loginManager?.authorizedURL ?? ""
        }
    }
    
    init() {
        status = MutableProperty(.initial)
        loggedUserPipe = BoolSignal.pipe()
        loginManager = AssemblerWrapper.shared.resolve(LoginService.self)
        databaseManager = AssemblerWrapper.shared.resolve(DatabaseService.self)
        postsRepository = AssemblerWrapper.shared.resolve(PostsRepositoryService.self)
        
        let isUserRequiredToLog = Property(initial: loginManager?.isUserLogged ?? false, then: loggedUserPipe.output)
        
        if var postsRepository = self.postsRepository {
            dataSource = postsRepository.dataSource.map({ (posts: [Post]) -> [PostSection] in
                return [PostSection(items: posts)]
            })
            postsRepository.expirationDelegate = self
        } else {
            OSLogger.dependencyInjectionLog(message: "Unable to resolve PostsRepositoryService dependency")
            dataSource = Observable.just([])
        }
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            disposable = status <~ SignalProducer.combineLatest(appDelegate.networkStatus.producer, isUserRequiredToLog.producer).map({ [weak self] (isNetworkAvailable: Bool?, isUserLogged: Bool) -> GalleryStatus in
                if let connectionAvailable = isNetworkAvailable {
                    switch (connectionAvailable, isUserLogged) {
                    case (true, true):
                        self?.postsRepository?.getPostsFromWebService()
                        return .fetchingServerData
                    case (true, false):
                        self?.deleteSavedPosts()
                        return .userNotLogged
                    case (false, true):
                        return .fetchingDbData
                    case (false, false):
                        return .networkNotAvailable
                    }
                } else {
                    return .initial
                }
            })
        }
    }
    
    deinit {
        if let d = disposable, !d.isDisposed {
            OSLogger.dataFlowLog(message: "Disposing GalleryViewModel")
            d.dispose()
        }
    }
    
    //MARK: Public Functions
    func saveUserToken(token: String) {
        loginManager?.saveAuthorizationToken(token: token)
        loggedUserPipe.input.send(value: true)
    }
    
    //MARK: Private Functions
    private func logOut() {
        OSLogger.dataFlowLog(message: "Logging out")
        loginManager?.deleteAuthorizationToken()
        loggedUserPipe.input.send(value: false)
    }
    
    private func deleteSavedPosts() {
        if let dbManager = self.databaseManager {
            do {
                try dbManager.deleteAllEntities().subscribeOn(subscriptionScheduler).subscribe().disposed(by: disposeBag)
                OSLogger.databaseLog(message: "Deleted Posts on database")
            } catch {
                OSLogger.errorLog(message: "Failed to delete Posts on database, error: \(error)")
            }
        } else {
            OSLogger.dependencyInjectionLog(message: "Unable to resolve DatabaseService dependency")
        }
    }
}

extension GalleryViewModel: ExpirationDelegate {
    func expireToken() {
        logOut()
    }
}
