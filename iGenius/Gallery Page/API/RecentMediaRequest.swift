//
//  RecentMediaRequest.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 01/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import OSLogger
import Foundation
import NetworkManager

class RecentMediaRequest: GetRequest<RecentMediaResponse> {
    init() {
        let host = "api.instagram.com"
        let version = "/v1"
        let path = "/users/self/media/recent/"
        
        var queryParameters: [String : CustomStringConvertible] = [:]
        if let loginManager = AssemblerWrapper.shared.resolve(LoginService.self), let token = loginManager.authorizationToken {
            queryParameters["access_token"] = token
        }
        
        super.init(host: host, path: path, version: version, queryParameters: queryParameters)
    }
    
    override func validateResponse(_ response: URLResponse) -> NSError? {
        guard let httpResponse = response as? HTTPURLResponse else {
            OSLogger.networkLog(message: "Received invalid response", access: .public, type: .debug)
            return NetworkError(errorType: .invalidResponse)
        }

        switch httpResponse.statusCode {
        case 200...399:
            return nil
        case 400...499:
            OSLogger.networkLog(message: "Token is not valid")
            return AppError(errorType: .tokenInvalid)
        default:
            OSLogger.networkLog(message: "Received HTTP Response with Error code \(httpResponse.statusCode)")
            return NetworkError(errorType: .invalidResponse)
        }
    }
}
