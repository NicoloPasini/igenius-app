//
//  RecentMediaResponse.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 01/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import OSLogger
import Foundation
import NetworkManager

struct RecentMediaResponse {
    private let data: [Post]
    
    init(posts: [Post]) {
        self.data = posts
    }
    
    func getPosts() -> [Post] {
        return data.filter { (post: Post) -> Bool in
            post.type == "image"
        }
    }
}

extension RecentMediaResponse: CustomDecodable {
    static func decode(_ data: Data) -> CustomDecodable? {
        let response = try? JSONDecoder().decode(RecentMediaResponse.self, from: data)
        
        if let mediaResponse = response {
            OSLogger.networkLog(message: "Recent Media Response contains \(mediaResponse.data.count) posts")
            return mediaResponse
        } else {
            OSLogger.networkLog(message: "Decoding of recent Media was not successful")
            return nil
        }
    }
}

