//
//  NetworkConnectionManager.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 07/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Network
import OSLogger
import Foundation
import ReactiveSwift

class NetworkConnectionManager: NetworkConnectionService {
    let networkStatus: Property<Bool?>
    
    private let networkMonitor: NWPathMonitor
    private let networkAvailablePipe: (output: BoolSignal, input: BoolSignal.Observer)
    
    init() {
        networkMonitor = NWPathMonitor()
        networkAvailablePipe = BoolSignal.pipe()
        networkStatus = Property(initial: nil, then: networkAvailablePipe.output)
        
        startNetworkMonitor()
    }
    
    func stopMonitoring() {
        networkMonitor.cancel()
    }
    
    private func startNetworkMonitor() {
        networkMonitor.pathUpdateHandler = { [weak self] path in
            if path.status == .satisfied {
                OSLogger.dataFlowLog(message: "Network available")
                self?.networkAvailablePipe.input.send(value: true)
            } else {
                OSLogger.dataFlowLog(message: "Network not available")
                self?.networkAvailablePipe.input.send(value: false)
            }
        }
        
        let networkMonitorQueue = DispatchQueue(label: "Network Monitor", qos: .background)
        networkMonitor.start(queue: networkMonitorQueue)
    }
}
