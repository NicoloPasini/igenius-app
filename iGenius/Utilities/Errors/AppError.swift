//
//  AppError.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 06/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation

private let AppDomain: String = "iGenius"

class AppError: NSError {
    convenience init(errorType: AppErrorType) {
        self.init(domain: AppDomain, code: errorType.rawValue, userInfo: [NSLocalizedDescriptionKey : String(describing: errorType)])
    }
}
