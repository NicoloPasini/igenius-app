//
//  AppErrorType.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 06/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation

enum AppErrorType: Int, Error {
    case dbDependency
    case tokenInvalid
    
    var description: String {
        switch self {
        case .dbDependency: return "Database Dependeny not resolved"
        case .tokenInvalid: return "The provided user token is not valid"
        }
    }
}
