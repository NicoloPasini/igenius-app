//
//  Registrable.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 07/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import Foundation

protocol Registrable {
    func register(viewType: Identifiable.Type)
    func register(_ nib: UINib?, forCellReuseIdentifier identifier: String)
}

extension Registrable {
    func register(viewType: Identifiable.Type) {
        register(viewType.nib(), forCellReuseIdentifier: viewType.identifier)
    }
}
