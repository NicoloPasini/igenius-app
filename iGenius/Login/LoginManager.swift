//
//  LoginManager.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 30/09/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import OSLogger
import Foundation
import KeychainItemWrapper

private let groupKey = "group.iGenius"
private let authTokenKey = "authToken"
private let authTokenKeychainKey = "token"

struct LoginManager: LoginService {
    //MARK: LoginService
    var isUserLogged: Bool {
        get {
            return getAuthorizationToken() == nil ? false : true
        }
    }
    
    var authorizedURL: String {
        get {
            return "https://oauth-server-68d1d.firebaseapp.com/authorized"
        }
    }
    
    var authenticationURL: URL? {
        get {
            return URL(string: "https://oauth-server-68d1d.firebaseapp.com/authentication.html")
        }
    }
    
    var tokenRegex: String {
        get {
            return "accessToken=[a-zA-Z0-9\\.]+?$"
        }
    }
    
    var authorizationToken: String? {
        get {
            return getAuthorizationToken()
        }
    }
    
    func saveAuthorizationToken(token: String) {
        let keychainWrapper = KeychainItemWrapper(identifier: authTokenKey, accessGroup: groupKey)
        keychainWrapper?.setObject(token, forKey: kSecValueData)
        keychainWrapper?.setObject(authTokenKeychainKey, forKey: kSecAttrService)
        
        OSLogger.dataFlowLog(message: "Stored access token \(token) in keychain")
    }
    
    func deleteAuthorizationToken() {
        let keychainWrapper = KeychainItemWrapper(identifier: authTokenKey, accessGroup: groupKey)
        keychainWrapper?.resetKeychainItem()
        
        OSLogger.dataFlowLog(message: "Delete access token stored in keychain")
    }
    
    //MARK: Private Functions
    private func getAuthorizationToken() -> String? {
        let keychainWrapper = KeychainItemWrapper(identifier: authTokenKey, accessGroup: groupKey)
        guard let token = keychainWrapper?.object(forKey: kSecValueData) as? String else {
            OSLogger.dataFlowLog(message: "No access token stored in keychain")
            return nil
        }
        
        OSLogger.dataFlowLog(message: "Retrieved saved access token: \(token)")
        
        return token.isEmpty ? nil : token
    }
}
