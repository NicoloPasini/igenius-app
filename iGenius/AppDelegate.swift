//
//  AppDelegate.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 30/09/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import OSLogger
import Swinject
import ReactiveSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var disposable: Disposable?
    let networkStatus: MutableProperty<Bool?> = MutableProperty(nil)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        var isTest: Bool = false
        var assemblies: [Assembly] = []
        
        if CommandLine.arguments.contains("--uitesting") {
            isTest = true
            assemblies = [UITestLoginAssembly(), UITestAvailableNetworkAssembly(), UITestWebRepositoryAssembly(), DatabaseAssembly()]
        } else if (CommandLine.arguments.contains("--logout-uitesting")) {
            isTest = true
            assemblies = [UITestLogoutAssembly(), UITestAvailableNetworkAssembly(), UITestWebRepositoryAssembly(), DatabaseAssembly()]
        } else if (CommandLine.arguments.contains("--connectionNotAvailable-uitesting")) {
            isTest = true
            assemblies = [UITestLoginAssembly(), UITestNetworkNotAvailableAssembly(), UITestWebRepositoryAssembly(), DatabaseAssembly()]
        } else {
            assemblies = [AppServicesAssembly(), RepositoriesAssembly(), DatabaseAssembly()]
        }
        
        AssemblerWrapper.shared.register(assemblies: assemblies)
        
        setupDatabase(application, isTest: isTest)
        
        startMonitoringConnectivity()
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    deinit {
        dispose()
    }
    
    //MARK: Public Functions
    func startMonitoringConnectivity() {
        if let networkConnection = AssemblerWrapper.shared.resolve(NetworkConnectionService.self) {
            disposable = networkConnection.networkStatus.producer.observe(on: QueueScheduler(qos: .background, name: "NetworkConnectionQueue")).startWithValues({ [weak self] (isConnectionAvailable: Bool?) in
                self?.networkStatus.value = isConnectionAvailable
            })
        } else {
            OSLogger.dependencyInjectionLog(message: "Unable to resolve NetworkConnectionService dependency")
        }
    }
    
    //MARK: Private Functions
    private func setupDatabase(_ application: UIApplication, isTest: Bool) {
        if let dbManager = AssemblerWrapper.shared.resolve(DatabaseService.self) {
            do {
                try dbManager.openDatabase(forApplication: application, isTest: isTest)
            } catch {
                OSLogger.databaseLog(message: "Error during database initialization: \(error.localizedDescription)")
            }
        }
    }
    
    private func dispose() {
        if let d = disposable, !d.isDisposed {
            d.dispose()
        }
    }
}

