//
//  DatabaseAssembly.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Swinject
import Foundation

class DatabaseAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DatabaseService.self) { _ in return GRDatabaseComponent() }.inObjectScope(.container)
    }
}
