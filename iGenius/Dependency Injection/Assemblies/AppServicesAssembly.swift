//
//  AppServicesAssembly.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 30/09/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Swinject
import Foundation

class AppServicesAssembly: Assembly {
    func assemble(container: Container) {
        container.register(LoginService.self) { _ in return LoginManager() }
        container.register(NetworkConnectionService.self) { _ in return NetworkConnectionManager() }.inObjectScope(.container)
    }
}
