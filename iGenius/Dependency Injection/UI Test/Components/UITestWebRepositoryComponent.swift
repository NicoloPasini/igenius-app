//
//  UITestWebRepositoryComponent.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation
import ReactiveSwift

fileprivate let post1 = Post(likes: 10, type: "image", comments: 25, postId: "1", caption: "Post 1", thumbImage: Image(width: 100, height: 200, url: ""), lowResImage: Image(width: 100, height: 200, url: ""), standardResImage: Image(width: 100, height: 200, url: ""))
fileprivate let post2 = Post(likes: 12, type: "image", comments: 43, postId: "2", caption: "Post 2", thumbImage: Image(width: 150, height: 210, url: ""), lowResImage: Image(width: 150, height: 210, url: ""), standardResImage: Image(width: 150, height: 210, url: ""))
fileprivate let post3 = Post(likes: 23, type: "image", comments: 5, postId: "3", caption: "Post 3", thumbImage: Image(width: 200, height: 150, url: ""), lowResImage: Image(width: 200, height: 150, url: ""), standardResImage: Image(width: 200, height: 150, url: ""))

class UITestWebRepositoryComponent: WebPostsRepositoryService {
    public func getPostsFromServer() -> SignalProducer<[Post], NSError> {
        return SignalProducer<[Post], NSError> { (observer, lifetime) in
            observer.send(value: [post1, post2, post3])
            observer.sendCompleted()
        }
    }
}
