//
//  UITestLogManagerComponent.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Swinject
import Foundation

class UITestLoginManagerComponent: LoginService {
    var isUserLogged: Bool
    let tokenRegex: String = ""
    let authorizedURL: String = ""
    var authorizationToken: String?
    let authenticationURL: URL? = nil
    
    init(loggedIn: Bool) {
        isUserLogged = loggedIn
    }
    
    func deleteAuthorizationToken() {
        authorizationToken = nil
    }
    
    func saveAuthorizationToken(token: String) {
        authorizationToken = token
    }
}
