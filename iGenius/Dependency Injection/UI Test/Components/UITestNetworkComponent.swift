//
//  UITestNetworkComponent.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation
import ReactiveSwift

class UITestNetworkComponent: NetworkConnectionService {
    let networkStatus: Property<Bool?>
    
    init(available: Bool) {
        networkStatus = Property(value: available)
    }
}
