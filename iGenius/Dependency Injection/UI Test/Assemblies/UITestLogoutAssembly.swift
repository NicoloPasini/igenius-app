//
//  UITestLogoutAssembly.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Swinject
import Foundation

class UITestLogoutAssembly: Assembly {
    func assemble(container: Container) {
        container.register(LoginService.self) { _ in return UITestLoginManagerComponent(loggedIn: false) }
    }
}

