//
//  UITestAvailableNetworkAssembly.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Swinject
import Foundation

class UITestAvailableNetworkAssembly: Assembly {
    func assemble(container: Container) {
        container.register(NetworkConnectionService.self) { _ in return UITestNetworkComponent(available: true) }
    }
}
