//
//  UITestWebRepositoryAssembly.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Swinject
import Foundation

class UITestWebRepositoryAssembly: Assembly {
    func assemble(container: Container) {
        container.register(PostsRepositoryService.self) { _ in return PostsRepository() }
        container.register(WebPostsRepositoryService.self) { _ in return UITestWebRepositoryComponent() }
    }
}
