//
//  LoginService.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 30/09/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation

protocol LoginService {
    var tokenRegex: String { get }
    var isUserLogged: Bool { get }
    var authorizedURL: String { get }
    var authenticationURL: URL? { get }
    var authorizationToken: String? { get }
    
    func deleteAuthorizationToken()
    func saveAuthorizationToken(token: String)
}
