//
//  DatabaseService.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import UIKit
import RxSwift

protocol DatabaseService {
    func getPosts() -> Observable<[Post]>
    func save(posts: [Post]) throws -> Completable
    func deleteAllEntities() throws -> Completable
    func openDatabase(forApplication application: UIApplication, isTest: Bool) throws
}
