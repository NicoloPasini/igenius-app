//
//  NetworkConnectionService.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 07/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol NetworkConnectionService {
    var networkStatus: Property<Bool?> { get }
}
