//
//  PostsRepositoryService.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import RxSwift
import Foundation

protocol PostsRepositoryService {
    var dataSource: Observable<[Post]> { get }
    var expirationDelegate: ExpirationDelegate? { get set}
    
    func getPostsFromWebService()
}

protocol ExpirationDelegate: class {
    func expireToken()
}
