//
//  WebPostsRepositoryService.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 06/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol WebPostsRepositoryService {
    func getPostsFromServer() -> SignalProducer<[Post], NSError>
}
