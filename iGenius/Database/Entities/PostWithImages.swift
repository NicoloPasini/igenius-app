//
//  PostWithImages.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 04/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import Foundation

struct PostWithImages: FetchableRecord, Decodable {
    var post: PostEntity
    var images: [ImageEntity]

    static func all() -> QueryInterfaceRequest<PostWithImages> {
        return PostEntity.including(all: PostEntity.images.forKey(CodingKeys.images)).asRequest(of: PostWithImages.self)
    }
}
