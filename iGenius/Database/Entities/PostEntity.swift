//
//  PostEntity.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import Foundation

struct PostEntity: Codable {
    let likes: Int
    let type: String
    let comments: Int
    let postId: String
    let caption: String
    
    init(from post: Post) {
        type = post.type
        likes = post.likes
        postId = post.postId
        comments = post.comments
        caption = post.caption ?? ""
    }
}

extension PostEntity: FetchableRecord, MutablePersistableRecord {
    static let databaseTableName = "post"
    
    //Reference to ImageEntity
    static let images = hasMany(ImageEntity.self)
    
    private enum Columns {
        static let type = Column(CodingKeys.type)
        static let likes = Column(CodingKeys.likes)
        static let postId = Column(CodingKeys.postId)
        static let caption = Column(CodingKeys.caption)
        static let comments = Column(CodingKeys.comments)
    }
}
