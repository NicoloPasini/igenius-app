//
//  ImageEntity.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import Foundation

enum ImageEntityType: String {
    case thumb = "thumbnail"
    case lowRes = "low_resolution"
    case standardRes = "standard_resolution"
}

struct ImageEntity: Codable {
    var id: Int64? //Used for table key in DB
    let width: Int
    let height: Int
    let url: String
    let type: String
    let postId: String
    
    init(from image: Image, withPostId referencedPostId: String, withType imageType: ImageEntityType) {
        id = nil
        url = image.url
        width = image.width
        height = image.height
        type = imageType.rawValue
        postId = referencedPostId
    }
}

extension ImageEntity: FetchableRecord, MutablePersistableRecord {
    static let databaseTableName = "image"
    
    static let post = belongsTo(PostEntity.self)
    
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let url = Column(CodingKeys.url)
        static let type = Column(CodingKeys.type)
        static let width = Column(CodingKeys.width)
        static let height = Column(CodingKeys.height)
        static let postId = Column(CodingKeys.postId)
    }
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}
