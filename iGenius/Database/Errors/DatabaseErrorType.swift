//
//  DatabaseErrorType.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 04/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation

enum DatabaseErrorType: Int, Error {
    case migration
    case dbLocation
    case accessPool
    case deleteFailed
    case insertPostFailed
    case insertImageFailed
    
    var description: String {
        switch self {
        case .migration: return "Migration failed"
        case .dbLocation: return "Cannot access DB location"
        case .accessPool: return "Unable to retrieve Pool for connection"
        case .deleteFailed: return "Error thrown while deleting all entities"
        case .insertPostFailed: return "Error thrown while inserting a Post Entity"
        case .insertImageFailed: return "Error thrown while inserting an Image Entity"
        }
    }
}
