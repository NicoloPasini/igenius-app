//
//  DatabaseError.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 04/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import Foundation

private let DatabaseDomain: String = "database"

class DatabaseError: NSError {
    public convenience init(errorType: DatabaseErrorType) {
        self.init(domain: DatabaseDomain, code: errorType.rawValue, userInfo: [NSLocalizedDescriptionKey : String(describing: errorType)])
    }
}
