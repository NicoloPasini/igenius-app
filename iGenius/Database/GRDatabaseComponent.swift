//
//  GRDatabaseComponent.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 02/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import UIKit
import RxGRDB
import RxSwift

class GRDatabaseComponent: DatabaseService {
    private var disposeBag = DisposeBag()
    private var postDAO: PostDAO = PostDAO(database: { fatalError("Database is uninitialized") })
    
    //MARK: Migration
    private var migrator: DatabaseMigrator {
        var migrator = DatabaseMigrator()
        
        migrator.registerMigration("create_post_image_tables") { db in
            try db.create(table: "post", body: { (t: TableDefinition) in
                t.column("postId", .text).primaryKey()
                
                t.column("type", .text).notNull().defaults(to: "")
                t.column("caption", .text).notNull().defaults(to: "")
                t.column("likes", .integer).notNull().defaults(to: 0)
                t.column("comments", .integer).notNull().defaults(to: 0)
            })
            
            try db.create(table: "image", body: { (t: TableDefinition) in
                t.autoIncrementedPrimaryKey("id")
                
                t.column("url", .text).notNull()
                t.column("type", .text).notNull()
                t.column("width", .integer).notNull()
                t.column("height", .integer).notNull()
                t.column("postId", .text).notNull().indexed().references("post", onDelete: .cascade)
            })
        }
        
        return migrator
    }
    
    //MARK: DatabaseService
    func openDatabase(forApplication application: UIApplication, isTest: Bool) throws {
        var config = Configuration()
        config.maximumReaderCount = 5
        config.label = "GalleryDatabase"
        #if DEBUG
        config.trace = { print($0) }
        #endif
        
        let databaseURL = try createDBLocationURL(isTest: isTest)
        try setUpDBPool(dbLocation: databaseURL, configuration: config, application: application)
    }
    
    func deleteAllEntities() throws -> Completable {
        return try postDAO.deleteAll()
    }
    
    func save(posts: [Post]) throws -> Completable {
        var postEntities: [PostEntity] = []
        var imageEntities: [ImageEntity] = []
        
        for post: Post in posts {
            let postEntity = PostEntity(from: post)
            let thumbImageEntity = ImageEntity(from: post.thumbImage, withPostId: post.postId, withType: .thumb)
            let lowResImageEntity = ImageEntity(from: post.lowResImage, withPostId: post.postId, withType: .lowRes)
            let standardResImageEntity = ImageEntity(from: post.standardResImage, withPostId: post.postId, withType: .standardRes)
            
            postEntities.append(postEntity)
            imageEntities.append(contentsOf: [thumbImageEntity, lowResImageEntity, standardResImageEntity])
        }
        
        return try postDAO.addPost(postEntities, withImages: imageEntities)
    }
    
    func getPosts() -> Observable<[Post]> {
        return postDAO.observeAllPosts().map { (posts: [PostWithImages]) -> [Post] in
            return posts.compactMap { (entity: PostWithImages) -> Post? in
                let thumbEntity = entity.images.first { (imageEntity: ImageEntity) -> Bool in
                    imageEntity.type == ImageEntityType.thumb.rawValue
                }
                let lowResEntity = entity.images.first { (imageEntity: ImageEntity) -> Bool in
                    imageEntity.type == ImageEntityType.lowRes.rawValue
                }
                let standardResEntity = entity.images.first { (imageEntity: ImageEntity) -> Bool in
                    imageEntity.type == ImageEntityType.standardRes.rawValue
                }
                
                if let thumb = thumbEntity, let lowRes = lowResEntity, let standardRes = standardResEntity {
                    let thumbImage = Image(width: thumb.width, height: thumb.height, url: thumb.url)
                    let lowResImage = Image(width: lowRes.width, height: lowRes.height, url: lowRes.url)
                    let standardResImage = Image(width: standardRes.width, height: standardRes.height, url: standardRes.url)
                    
                    return Post(likes: entity.post.likes, type: entity.post.type, comments: entity.post.comments, postId: entity.post.postId, caption: entity.post.caption, thumbImage: thumbImage, lowResImage: lowResImage, standardResImage: standardResImage)
                } else {
                    return nil
                }
            }
        }
    }
    
    //MARK: Private Functions
    private func createDBLocationURL(isTest: Bool) throws -> URL {
        let dbName = isTest ? "dbTest.sqlite" : "db.sqlite"
        do {
            return try FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true).appendingPathComponent(dbName)
        } catch {
            throw DatabaseError(errorType: .dbLocation)
        }
    }
    
    private func setUpDBPool(dbLocation: URL, configuration: Configuration, application: UIApplication) throws {
        do {
            let pool = try DatabasePool(path: dbLocation.path, configuration: configuration)
            pool.setupMemoryManagement(in: application)
            
            do {
                try migrator.migrate(pool)
            } catch {
                
            }
            
            postDAO = PostDAO(database: { pool })
        } catch {
            throw DatabaseError(errorType: .accessPool)
        }
    }
}
