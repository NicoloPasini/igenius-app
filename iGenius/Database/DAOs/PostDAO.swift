//
//  PostDAO.swift
//  iGenius
//
//  Created by Pasini, Nicolò on 04/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

import GRDB
import RxGRDB
import RxSwift
import Foundation

struct PostDAO {
    private let database: () -> DatabaseWriter
    private let databaseScheduler: SchedulerType
    
    init(database: @escaping () -> DatabaseWriter) {
        self.database = database
        databaseScheduler = ConcurrentDispatchQueueScheduler(qos: .default)
    }
    
    //MARK: Modify Post Entities
    func deleteAll() throws -> Completable {
        return database().rx.write(observeOn: databaseScheduler, updates: _deleteAll)
    }
    
    func addPost(_ posts: [PostEntity], withImages images: [ImageEntity]) throws -> Completable {
        return database().rx.write(observeOn: databaseScheduler) { db in
            for var postEntity: PostEntity in posts {
                do {
                    try postEntity.insert(db)
                } catch {
                    throw DatabaseError(errorType: .insertPostFailed)
                }
            }
            
            for var imageEntity: ImageEntity in images {
                do {
                    try imageEntity.insert(db)
                } catch {
                    throw DatabaseError(errorType: .insertImageFailed)
                }
            }
        }
    }
    
    // MARK: Access Post Entities
    func observeAllPosts() -> Observable<[PostWithImages]> {
        return PostWithImages.all().rx.observeAll(in: database(), observeOn: databaseScheduler)
    }
    
    // MARK: Private Functions
    private func _deleteAll(_ db: Database) throws {
        do {
            try PostEntity.deleteAll(db)
            try ImageEntity.deleteAll(db)
        } catch {
            throw DatabaseError(errorType: .deleteFailed)
        }
    }
}
