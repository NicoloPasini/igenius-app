//
//  RecentMediaRequestTest.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Quick
import Nimble
import Foundation

class RecentMediaRequestTest: QuickSpec {
    override func spec() {
        context("Testing the Get Recent Media API request"){
            describe("an istance of the request should contain correct endpoint and parameters"){
                beforeEach {
                    AssemblerWrapper.shared.register(assemblies: [TestLogInAssembly()])
                }
                
                it("when the user is not logged in the request should not contain the query parameter with the access token"){
                    let request = RecentMediaRequest()
                    
                    expect(request.host).to(equal("api.instagram.com"))
                    expect(request.version).to(equal("/v1"))
                    expect(request.path).to(equal("/users/self/media/recent/"))
                    expect(request.queryParameters).notTo(beNil())
                    
                    expect(request.queryParameters?["access_token"]).to(beNil())
                }
                
                it("when the user is logged in the request should contain the query parameter with the access token"){
                    let authToken = "testAuthToken"
                    
                    if let loginManager = AssemblerWrapper.shared.resolve(LoginService.self) {
                        loginManager.saveAuthorizationToken(token: authToken)
                    }
                    
                    let request = RecentMediaRequest()
                    
                    expect(request.host).to(equal("api.instagram.com"))
                    expect(request.version).to(equal("/v1"))
                    expect(request.path).to(equal("/users/self/media/recent/"))
                    expect(request.queryParameters).notTo(beNil())
                    
                    if let queryParameter = request.queryParameters?["access_token"] as? String {
                        expect(queryParameter).to(equal(authToken))
                    } else {
                        fail("Page parameter not present")
                    }
                }
            }
        }
    }
}
