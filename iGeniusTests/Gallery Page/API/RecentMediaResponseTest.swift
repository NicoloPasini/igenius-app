//
//  RecentMediaResponseTest.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Quick
import Nimble
import Foundation

class RecentMediaResponseTest: QuickSpec {
    override func spec() {
        context("Testing the Get Recent Media API response") {
            describe("the response should not be decoded"){
                it("with empty data"){
                    let response = RecentMediaResponse.decode(Data())
                    expect(response).to(beNil())
                }
            }
            
            describe("the response should be decoded") {
                it("with correct json to parse"){
                    let path = Bundle(for: type(of: self)).path(forResource: "validRecentMediaResponse", ofType: "json") ?? ""
                    let data = (try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe))
                    
                    guard let dataObject = data else {
                        fail("Wrong json file path")
                        return
                    }
                    
                    let response = RecentMediaResponse.decode(dataObject)
                    expect(response).notTo(beNil())
                    expect(response).to(beAnInstanceOf(RecentMediaResponse.self))
                    
                    let mediaResponse = response as! RecentMediaResponse
                    let posts = mediaResponse.getPosts()
                    expect(posts.count).to(equal(19))
                    
                    for post in posts {
                        expect(post.likes).notTo(beNil())
                        expect(post.postId).notTo(beNil())
                        expect(post.comments).notTo(beNil())
                        expect(post.thumbImage.url).notTo(beNil())
                        expect(post.thumbImage.width).notTo(beNil())
                        expect(post.thumbImage.height).notTo(beNil())
                        expect(post.lowResImage.url).notTo(beNil())
                        expect(post.lowResImage.width).notTo(beNil())
                        expect(post.lowResImage.height).notTo(beNil())
                        expect(post.standardResImage.url).notTo(beNil())
                        expect(post.standardResImage.width).notTo(beNil())
                        expect(post.standardResImage.height).notTo(beNil())
                    }
                }
            }
            
            describe("the response should not be decoded") {
                it("with a json where all the Beer objects have not all the mandatory fields"){
                    let path = Bundle(for: type(of: self)).path(forResource: "invalidRecentMediaResponse", ofType: "json") ?? ""
                    let data = (try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe))
                    
                    guard let dataObject = data else {
                        fail("Wrong json file path")
                        return
                    }
                    
                    let response = RecentMediaResponse.decode(dataObject)
                    expect(response).to(beNil())
                }
            }
        }
    }
}

