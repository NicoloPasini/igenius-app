//
//  GalleryViewModelTest.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Quick
import Nimble
import RxSwift
import Foundation

class GalleryViewModelTest: QuickSpec {
    override func spec() {
        context("Testing the GalleryViewModel") {
            var viewModel: GalleryViewModel!
            
            describe("when a new instance of the ViewModel is created and the user is not logged in but network connection is available") {
                it("the value of the status should be userNotlogged") {
                    AssemblerWrapper.shared.register(assemblies: [TestLogInAssembly(), NetworkAvailableAssembly()])
                    
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.startMonitoringConnectivity()
                    } else {
                        fail("AppDelegate unresolved")
                    }
                    
                    viewModel = GalleryViewModel()
                    expect(viewModel.status.value).toEventually(equal(GalleryStatus.userNotLogged), timeout: 10)
                }
            }
            
            describe("when a new instance of the ViewModel is created and the user is not logged in and network connection is not available") {
                it("the value of the status should be networkNotAvailable") {
                    AssemblerWrapper.shared.register(assemblies: [TestLogInAssembly(), NetworkNotAvailableAssembly()])
                    
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.startMonitoringConnectivity()
                    } else {
                        fail("AppDelegate unresolved")
                    }
                    
                    viewModel = GalleryViewModel()
                    expect(viewModel.status.value).toEventually(equal(GalleryStatus.networkNotAvailable), timeout: 10)
                }
            }
            
            describe("when a new instance of the ViewModel is created and the user is logged in and network connection is not available") {
                it("the value of the status should be fetchingDbData") {
                    AssemblerWrapper.shared.register(assemblies: [TestLogInAssembly(), NetworkNotAvailableAssembly()])
                    
                    if let loginManager = AssemblerWrapper.shared.resolve(LoginService.self) {
                        loginManager.saveAuthorizationToken(token: "testAuthToken")
                    } else {
                        fail("LoginManager unresolved")
                    }
                    
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.startMonitoringConnectivity()
                    } else {
                        fail("AppDelegate unresolved")
                    }
                    
                    viewModel = GalleryViewModel()
                    expect(viewModel.status.value).toEventually(equal(GalleryStatus.fetchingDbData), timeout: 10)
                }
            }
            
            describe("when a new instance of the ViewModel is created and the user is logged in and the network connection is available") {
                it("the value of the status should be fetchingServerData") {
                    AssemblerWrapper.shared.register(assemblies: [TestLogInAssembly(), NetworkAvailableAssembly(), TestRepositoriesAssembly(), TestDatabaseAssembly()])
                    
                    if let loginManager = AssemblerWrapper.shared.resolve(LoginService.self) {
                        loginManager.saveAuthorizationToken(token: "testAuthToken")
                    } else {
                        fail("LoginManager unresolved")
                    }
                    
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        appDelegate.startMonitoringConnectivity()
                    } else {
                        fail("AppDelegate unresolved")
                    }
                    
                    viewModel = GalleryViewModel()
                    
                    if viewModel.status.value != .fetchingDbData {
                        expect(viewModel.status.value).toEventually(equal(GalleryStatus.fetchingServerData), timeout: 10)
                    }
                }
            }
        }
    }
}
