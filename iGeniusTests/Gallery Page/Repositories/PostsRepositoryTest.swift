//
//  PostsRepositoryTest.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Quick
import Nimble
import RxSwift
import Foundation

class PostsRepositoryTest: QuickSpec {
    override func spec() {
        context("Testing the PostsRepository") {
            let disposeBag = DisposeBag()
            var postRepository: PostsRepositoryService!
            
            describe("when the database is empty and a new instance of the PostsRepository is created") {
                beforeEach {
                    AssemblerWrapper.shared.register(assemblies: [TestRepositoriesAssembly(), TestDatabaseAssembly()])
                    
                    if let repository = AssemblerWrapper.shared.resolve(PostsRepositoryService.self) {
                        postRepository = repository
                    } else {
                        fail("PostRepository unresolved")
                    }
                }
                
                it("its datasource should be empty") {
                    waitUntil (timeout: 5) { done in
                        postRepository.dataSource.subscribe(onNext: { (posts: [Post]) in
                            expect(posts).to(equal([]))
                            done()
                        }).disposed(by: disposeBag)
                    }
                }
                
                it("its datasource should be updated with the values retrieved frombackend once required") {
                    waitUntil (timeout: 5) { done in
                        postRepository.getPostsFromWebService()
                        postRepository.dataSource.subscribe(onNext: { (posts: [Post]) in
                            if posts.count > 0 {
                                expect(posts).to(equal(PostData.webPosts))
                                done()
                            }
                        }).disposed(by: disposeBag)
                    }
                }
            }
            
            describe("when the database is not empty and a new instance of the PostsRepository is created") {
                beforeEach {
                    AssemblerWrapper.shared.register(assemblies: [TestRepositoriesAssembly(), TestDatabaseAssembly()])
                    
                    if let repository = AssemblerWrapper.shared.resolve(PostsRepositoryService.self) {
                        postRepository = repository
                    } else {
                        fail("PostRepository unresolved")
                    }
                    
                    if let database = AssemblerWrapper.shared.resolve(DatabaseService.self) {
                        try? database.save(posts: PostData.dbPosts).subscribe().disposed(by: disposeBag)
                    } else {
                        fail("Database unresolved")
                    }
                }
                
                it("its datasource should contains the database values") {
                    waitUntil (timeout: 5) { done in
                        postRepository.dataSource.subscribe(onNext: { (posts: [Post]) in
                            expect(posts).to(equal(PostData.dbPosts))
                            done()
                        }).disposed(by: disposeBag)
                    }
                }

                it("its datasource should be updated with the values retrieved frombackend once required") {
                    AssemblerWrapper.shared.register(assemblies: [TestRepositoriesAssembly(), TestDatabaseAssembly()])

                    if let database = AssemblerWrapper.shared.resolve(DatabaseService.self) {
                        try? database.save(posts: PostData.dbPosts).subscribe().disposed(by: disposeBag)
                    } else {
                        fail("Database unresolved")
                    }

                    waitUntil (timeout: 5) { done in
                        postRepository.getPostsFromWebService()
                        postRepository.dataSource.subscribe(onNext: { (posts: [Post]) in
                            if posts.count > 0 && posts != PostData.dbPosts {
                                expect(posts).to(equal(PostData.webPosts))
                                done()
                            }
                        }).disposed(by: disposeBag)
                    }
                }
            }
            
            describe("when the the token expires") {
                let repositoryDelegate = RepositoryDelegate()
                
                beforeEach {
                    AssemblerWrapper.shared.register(assemblies: [ErrorRepositoriesAssembly(), TestDatabaseAssembly()])
                    
                    if let repository = AssemblerWrapper.shared.resolve(PostsRepositoryService.self) {
                        postRepository = repository
                        postRepository.expirationDelegate = repositoryDelegate
                    } else {
                        fail("PostRepository unresolved")
                    }
                    
                    if let database = AssemblerWrapper.shared.resolve(DatabaseService.self) {
                        try? database.save(posts: PostData.dbPosts).subscribe().disposed(by: disposeBag)
                    } else {
                        fail("Database unresolved")
                    }
                }
                
                it("the PostRepository should notify its delegate and the datasource should be deleted") {
                    postRepository.getPostsFromWebService()
                    
                    expect(repositoryDelegate.isTokenExpired).toEventually(equal(true), timeout: 5)
                }
            }
        }
    }
}
