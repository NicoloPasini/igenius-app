//
//  RepositoryDelegate.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

class RepositoryDelegate: ExpirationDelegate {
    var isTokenExpired: Bool = false
    
    func expireToken() {
        isTokenExpired = true
    }
}
