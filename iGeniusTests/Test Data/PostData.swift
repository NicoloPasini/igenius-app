//
//  PostData.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Foundation

let post1 = Post(likes: 10, type: "image", comments: 25, postId: "1", caption: "Post 1", thumbImage: Image(width: 100, height: 200, url: ""), lowResImage: Image(width: 100, height: 200, url: ""), standardResImage: Image(width: 100, height: 200, url: ""))
let post2 = Post(likes: 12, type: "image", comments: 43, postId: "2", caption: "Post 2", thumbImage: Image(width: 150, height: 210, url: ""), lowResImage: Image(width: 150, height: 210, url: ""), standardResImage: Image(width: 150, height: 210, url: ""))
let post3 = Post(likes: 23, type: "image", comments: 5, postId: "3", caption: "Post 3", thumbImage: Image(width: 200, height: 150, url: ""), lowResImage: Image(width: 200, height: 150, url: ""), standardResImage: Image(width: 200, height: 150, url: ""))

let dbPost1 = Post(likes: 10, type: "image", comments: 25, postId: "1", caption: "Post 1", thumbImage: Image(width: 100, height: 200, url: ""), lowResImage: Image(width: 100, height: 200, url: ""), standardResImage: Image(width: 100, height: 200, url: ""))
let dbPost2 = Post(likes: 12, type: "image", comments: 43, postId: "2", caption: "Post 2", thumbImage: Image(width: 150, height: 210, url: ""), lowResImage: Image(width: 150, height: 210, url: ""), standardResImage: Image(width: 150, height: 210, url: ""))

class PostData {
    static let dbPosts = [dbPost1, dbPost2]
    static let webPosts = [post1, post2, post3]
}
