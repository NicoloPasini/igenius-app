//
//  ErrorRepositoriesAssembly.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Swinject
import Foundation

class ErrorRepositoriesAssembly: Assembly {
   func assemble(container: Container) {
        container.register(PostsRepositoryService.self) { _ in return PostsRepository() }
        container.register(WebPostsRepositoryService.self) { _ in return ErrorWebRepository() }
    }
}
