//
//  NetworkAvailableAssembly.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Swinject
import Foundation

class NetworkAvailableAssembly: Assembly {
    func assemble(container: Container) {
        container.register(NetworkConnectionService.self) { _ in return NetworkConnectionAvailableComponent() }
    }
}
