//
//  NetworkConnectionAvailableComponent.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Foundation
import ReactiveSwift

class NetworkConnectionAvailableComponent: NetworkConnectionService {
    let networkStatus: Property<Bool?> = Property(value: true)
}
