//
//  NetworkConnectionNotAvailableComponent.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Foundation
import ReactiveSwift

class NetworkConnectionNotAvailableComponent: NetworkConnectionService {
    let networkStatus: Property<Bool?> = Property(value: false)
}
