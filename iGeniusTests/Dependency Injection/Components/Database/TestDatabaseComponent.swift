//
//  TestDatabaseComponent.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import RxSwift
import Foundation

class TestDatabaseComponent: DatabaseService {
    private var storedPosts: [Post] = []
    private let postSubject: BehaviorSubject<[Post]> = BehaviorSubject(value: [])
    
    func getPosts() -> Observable<[Post]> {
        return postSubject.asObservable()
    }
    
    func save(posts: [Post]) throws -> Completable {
        return Completable.create { [weak self] completable in
            self?.storedPosts.append(contentsOf: posts)
            self?.postSubject.onNext(self?.storedPosts ?? [])
            completable(.completed)
            return Disposables.create {}
        }
    }
    
    func deleteAllEntities() throws -> Completable {
        return Completable.create { [weak self] completable in
            self?.storedPosts = []
            self?.postSubject.onNext([])
            completable(.completed)
            return Disposables.create {}
        }
    }
    
    func openDatabase(forApplication application: UIApplication, isTest: Bool) throws {
    }
}
