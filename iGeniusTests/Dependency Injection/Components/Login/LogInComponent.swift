//
//  LogInComponent.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 12/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Foundation

class LogInComponent: LoginService {
    let tokenRegex: String = ""
    let authorizedURL: String = ""
    var authorizationToken: String?
    let authenticationURL: URL? = nil
    
    var isUserLogged: Bool {
        get {
            return (authorizationToken == nil) ? false : true
        }
    }
    
    func deleteAuthorizationToken() {
        authorizationToken = nil
    }
    
    func saveAuthorizationToken(token: String) {
        authorizationToken = token
    }
}
