//
//  ErrorWebRepository.swift
//  iGeniusTests
//
//  Created by Pasini, Nicolò on 13/10/2019.
//  Copyright © 2019 Pasini, Nicolò. All rights reserved.
//

@testable import iGenius

import Foundation
import ReactiveSwift

class ErrorWebRepository: WebPostsRepositoryService {
    //MARK: Public Functions
    public func getPostsFromServer() -> SignalProducer<[Post], NSError> {
        return SignalProducer<[Post], NSError> { (observer, lifetime) in
            observer.send(error: AppError(errorType: .tokenInvalid))
            observer.sendCompleted()
        }
    }
}
